---
layout: page
parent: notes
title: Schokokuchen
abstract: "Ein Rezept für einen einfachen Schokokuchen"
date:   2016-08-28 20:08:00
update: 2016-08-28 20:08:00
---

Als ich vor einiger Zeit mal wieder einen Anlass hatte einen Kuchen zu backen und ich Schokokuchen sowieso mag, habe ich mich nach passenden Rezepten umgesehen und bin auf drei Rezepte gestoßen, ein [Schokokuchen mit Nüssen](http://www.chefkoch.de/rezepte/569031155564178/Schokokuchen-mit-Nuessen), ein [Türkicher Schokoladenkuchen](http://www.chefkoch.de/rezepte/647321165995536/Tuerkischer-Schokoladenkuchen) und einfach ein [Schokokuchen](http://www.chefkoch.de/rezepte/1609441268234831/Schokokuchen).
Da mir alle Rezepte zum Teil zugesagt haben, aber keins vollständig habe ich daraus folgende Zusammenstellung komponiert:


|200 g |  Mehl |
|200 g |  Zucker |
|200 g |  Nüsse, Mandeln oder ähnlich, gemahlene |
| 80 g |  Kakaopulver |
|1 Pck |  Backpulver |
|1 Pck |  Vanillezucker |
|1 Prs |  Salz |
|  4-5 |  Eier |
|200 g |  Butter |


Ich glaube die 200 g Mehl, Zucker und Nüsse entsprechen jeweils etwa 2 Gläsern, Kakaopulver nach Gefühl wohl etwa ne halbe Tasse und dann kann man das ganze auch ohne Waage backen.
In zwei der Ausgangsrezepte kommen noch 200 ml Wasser bzw. 1 Glas Milch dazu, die Komponente hatte ich aber vergessen und der Kuchen hat trotzdem super geschmeckt.
