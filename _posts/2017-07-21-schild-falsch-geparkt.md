---
layout: status
date:   2017-07-21 11:25:00+02:00
author: white_gecko
---

.@Poliauwei was macht man denn mit falsch geparkten Schildern auf dem #Fahrradweg auf der Prager Straße in @StadtLeipzig?

<img src="/img/2017-07-21-Schild.jpg" alt="Verkehrsschild für den Autoverkehr mitten auf dem Fahrradweg" style="width: 30%" /><img src="/img/2017-07-21-Fahrradweg.jpg" alt="Nutzungspflicht für Fahrräder ist angeordnet" style="width: 30%" />

[https://twitter.com/white_gecko/status/888325725811613696](https://twitter.com/white_gecko/status/888325725811613696)
